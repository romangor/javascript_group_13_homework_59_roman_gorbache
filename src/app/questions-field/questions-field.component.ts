import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../shared/question.service';
import { Question } from '../shared/question.model';

@Component({
  selector: 'app-questions-field',
  templateUrl: './questions-field.component.html',
  styleUrls: ['./questions-field.component.css']
})
export class QuestionsFieldComponent implements OnInit {
  questions!: Question[];
  wrongAnswer!: number;
  constructor(private questionsService: QuestionService) { }

  ngOnInit(): void {
    this.questions = this.questionsService.getQuestions();
    this.questionsService.changeQuestions.subscribe( (questions: Question[]) => {
      this.questions = questions;
    });
    this.wrongAnswer = this.questionsService.wrongAnswers;
    this.questionsService.countAnswers.subscribe( (answer: number) => {
      this.wrongAnswer = answer;
    });
  }

}
