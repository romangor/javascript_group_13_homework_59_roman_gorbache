import { Component, Input } from '@angular/core';
import { Question } from '../../shared/question.model';
import { QuestionService } from '../../shared/question.service';

@Component({
  selector: 'app-question-card',
  templateUrl: './question-card.component.html',
  styleUrls: ['./question-card.component.css']
})
export class QuestionCardComponent {
  @Input() question!: Question;

  userAnswer: string = '';
  countAnswer:number = 0;
  constructor(private questionService: QuestionService) { }

   onClick() {
    if (this.userAnswer === this.question.answer){
      this.question.statusAnswer = 'correct';
      this.countAnswer++;
      this.questionService.countAnswer(this.countAnswer);
    } else {
      this.question.statusAnswer = 'inCorrect';
    }
    this.questionService.checkAnswer(this.question);
  }

}
