import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appShadow]'
})
export class ShadowDirective{
  @Input() set appShadow(shadowClass:string){
    if (shadowClass === 'correct'){
      this.shadow = shadowClass;
    } if (shadowClass === 'inCorrect'){
      this.shadow = shadowClass;
    } if (shadowClass === 'noAnswer'){
      this.shadow = shadowClass;
    }
  }

  shadow = 'noAnswer';

  constructor(private element: ElementRef, private renderer: Renderer2) {
  }

  @HostListener( 'mousemove' ) addClass() {
    this.renderer.addClass(this.element.nativeElement, this.shadow);
  }
  @HostListener( 'mouseleave' ) removeClass() {
    this.renderer.removeClass(this.element.nativeElement, this.shadow);
  }
}
