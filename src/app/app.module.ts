import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { QuestionsFieldComponent } from './questions-field/questions-field.component';
import { QuestionCardComponent } from './questions-field/question-card/question-card.component';
import { QuestionService } from './shared/question.service';
import { FormsModule } from '@angular/forms';
import { ShadowDirective } from './directives/shadow.directive';
import { HideElementDirective } from './directives/hide-element.directive';

@NgModule({
  declarations: [
    AppComponent,
    QuestionsFieldComponent,
    QuestionCardComponent,
    ShadowDirective,
    HideElementDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
