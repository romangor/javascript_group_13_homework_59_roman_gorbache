import { Question } from './question.model';
import { EventEmitter } from '@angular/core';

export class QuestionService {
  changeQuestions = new EventEmitter<Question[]>();
  questionAnswer = new EventEmitter<Question>();
  countAnswers = new EventEmitter<number>();

  private questions: Question[] = [new Question('Процесс поиска и устранения ошибок в программе?', 'отладка', 'qwerty', 'not Answer'),
    new Question('Графическое представление программы?', 'блок-схема', '', 'not Answer'),
    new Question('Конечный набор шагов, которые при следовании им решают какую-то задачу.?', 'алгоритм', '', 'not Answer'),
    new Question('Языковая конструкция, которая может определять участок программы для многократного повторения и количество этих повторений?', 'цикл', '', 'not Answer'),
    new Question('Набор смежных областей памяти, которые хранят данные определенного типа?', 'массив', '', 'not Answer'),];

  wrongAnswers: number = 0;

  getQuestions() {
    return this.questions.slice();
  }

  countAnswer(number:number) {
    this.countAnswers.emit(number);
  }

  checkAnswer(question: Question){
    this.questionAnswer.emit(question);
  }

}
