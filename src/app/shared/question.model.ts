export class Question {
  constructor(
    public question: string,
    public answer: string,
    public prompt: string,
    public statusAnswer: string,
  ) {}
}
